#!/bin/bash
# this is the wrapper bash script for the Blender Swift workflow

echo "Running Blender Workflow"

#inputs
blend=$1
textures=$2
frames=$3
#outputs
renderings=$4
animation=$5

# copy the input files to the correct names
cp $blend ./in.blend;cp $textures ./textures.tgz;

# run swift
swift render.swift -frames=$frames

# generate the animation
convert -delay 5 -loop 0 results/*.png $animation

# tar the result images
tar czf $renderings results

echo "Rendering Workflow Complete"
