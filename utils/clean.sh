# this script cleans up files after a run

rm results -R
rm run* -R
rm swiftwork -R
rm *.kml
rm in.blend
rm textures.tgz
rm renders.tgz
rm animation.gif