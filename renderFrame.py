# Render all cameras or cameras containing text given with command line argument "cameras".
# Example:
# Let's say test.blend file contains cameras "east.01", "east.02", "west.01", "west.02"
# By executing command "blender -b your_file.blend -P render_all_cameras.py" all 4 cameras will be rendered.
# Command "blender -b your_file.blend -P render_all_cameras.py  cameras=east" will render "east.01" and "east.02" cameras.
# Command "blender -b your_file.blend -P render_all_cameras.py  cameras=01" will render "east.01" and "west.01.02" cameras.


import bpy, bgl, blf,sys
from bpy import data, ops, props, types, context
import os 

frame=int(sys.argv[7])

# define the camera here
cameraNames='Camera.002'

# Loop all command line arguments and try to find "cameras=east" or similar
for arg in sys.argv:
    words=arg.split('=')
    if ( words[0] == 'cameras'):
     cameraNames = words[1]

print('rendering cameras containing [' + cameraNames + ']')

print('\nPrint Scenes...')
sceneKey = bpy.data.scenes.keys()[1]
print('Using Scene['  + sceneKey + ']')


# Loop all objects and try to find Cameras
print('Looping Cameras')
c=0
for obj in bpy.data.objects:
    # Find cameras that match cameraNames
    if ( obj.type =='CAMERA') and ( cameraNames == '' or obj.name.find(cameraNames) != -1) :
      print("Rendering scene["+sceneKey+"] with Camera["+obj.name+"]")

      # Set Scenes camera and output filename
      bpy.data.scenes[sceneKey].render.engine = 'CYCLES'
      bpy.data.scenes[sceneKey].camera = obj
      bpy.data.scenes[sceneKey].render.image_settings.file_format = 'PNG'
      bpy.context.scene.render.resolution_x = 600
      bpy.context.scene.render.resolution_y = 400
      
      # sample through the frame here - this is where we parallelize
      frameMax=bpy.data.scenes[sceneKey].frame_end

      bpy.data.scenes[sceneKey].render.filepath = "//results/out"
      bpy.data.scenes[sceneKey].frame_set(frame)

      bpy.ops.render.render(write_still=True) # render still

print('Done!')
