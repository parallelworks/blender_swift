#! /bin/bash

blend=$(basename $1)
frame=$2
out=$3

tar xzvf textures.tgz

docker run --rm -v $PWD/results:/tmp -v $PWD:/media/ ikester/blender -b /media/$blend -P "/media/renderFrame.py" -- $frame

mv results/out.png $out
